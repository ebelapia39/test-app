const express = require('express');
const cors = require('cors')
const cookieParser = require('cookie-parser');

require('dotenv').config();

const app = express();

const {
  PORT,
  NODE_ENV,
} = process.env;

app.use(cors({
  origin: 'http://localhost:8080',
  optionsSuccessStatus: 200
}))

app.use(express.json());

app.use(express.urlencoded({
  extended: false,
}));

app.use(cookieParser());

const routes = require('./routes');

app.use('/api/v1.0', routes);

app.listen(PORT, async (error) => {
  if (error) console.log(error);
  console.log(`NODE_ENV = ${NODE_ENV}`);
  console.log(`Запуск сервера на ${PORT} порту`);
});

process.on('uncaughtException', (err) => {
  console.log('Caught exception: ', err);
});

module.exports = app;
