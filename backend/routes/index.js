const express = require('express');
const path = require('path');
const router = express.Router();

const {
  Validator,
  ValidationError,
} = require('express-json-validator-middleware');

const validator = new Validator({
  allErrors: true,
});

const validatorValidate = validator.validate;

const {
  requireInFolder,
  requireFolder
} = require('../helpers');

const ctrl = requireFolder(path.join(__dirname, 'controllers'))
const validators = requireInFolder(path.join(__dirname, 'validate'));

router
  .post('/sum', validatorValidate({
    body: validators.sum,
  }), ctrl.cart.sum)

router.use(async (err, req, res, next) => {
  if (err instanceof ValidationError) {
    res.status(400).json({
      ok: false,
      err,
      message: 'Неверный формат входных данных',
    });
    next();
  } else next(err);
});

module.exports = router;