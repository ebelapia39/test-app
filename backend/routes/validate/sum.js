module.exports = {
    type: 'array',
    contains: {
        type: 'object',
        properties: {
            name: {
                'type': 'string'
            },
            quantity: {
                'type': 'number'
            },
            currency: {
                'type': 'string',
                'enum': ['rub', 'eur', 'usd']
            },
            price: {
                'type': 'number',
            },
        }
    }
};