const {
  axiosInstance
} = require('../../../helpers')
const axios = axiosInstance()

module.exports = async function (req, res, next) {
  try {
    const Body = req.body;
    let result = {
      RUB: 0,
      EUR: 0,
      USD: 0
    }

    let valutes = await axios.get('https://www.cbr-xml-daily.ru/daily_json.js')
    valutes = valutes.data.Valute

    result.RUB = Body.reduce((acc, article) => {
      return acc + (article.price * article.quantity)
    },0)

    for (let valute in result) {
      if(valute !== 'RUB') {
        if(valutes[valute]) {
          result[valute] = (result.RUB / valutes[valute].Value)
        }
      }
    }

    res.status(200).json({
      ok: true,
      data: result,
      message: 'Успешный подсчет корзины'
    })
  } catch (error) {
    console.log(error)
    res.status(400).json({
      ok: false,
      message: error.message
    })
  }
};