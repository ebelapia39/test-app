import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

function getUrl (path) {
  return `http://localhost:3000/api/v1.0/${path}`
}

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {
    GET_SUM: async (state, payload) => {
      let data = await axios.post(getUrl(`sum`), payload)
      return data.data
    }
  }
})
